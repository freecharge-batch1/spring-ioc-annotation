package com.freecharge.spring.ioc.annotations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.List;
@Component
public class Employee {
    private int id;
    private String name;
    private String email;
    private String contactNo;
   /* @Autowired
    @Qualifier("officeaddress")
    private Address address;*/


    @Autowired
    private List<Address> addressList;

    @Autowired

    public Employee(List<Address> addressList){
        this.addressList = addressList;
    }

   // @Autowired
    public Employee(){
        System.out.println("No-arg contstructor");
    }

   /* public Employee(int id, String name, String email, String contactNo, List<Address> addressList) {
        System.out.println("Employee(int,String,String,String)");
        this.id = id;
        this.name = name;
        this.email = email;
        this.contactNo = contactNo;
        this.addressList = addressList;

    }*/

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContactNo() {
        return contactNo;
    }

    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }

   /* public List<Address> getAddressList() {
        return addressList;
    }

    public void setAddressList(List<Address> addressList) {
        this.addressList = addressList;
    }
*/
    public void init(){
        System.out.println("initialization code");
    }
    public void cleanup(){
        System.out.println("clean up code");
    }

    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", contactNo='" + contactNo + '\'' +
               ", addressList=" + addressList +
                '}';
    }
}
