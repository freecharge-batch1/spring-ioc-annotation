package com.freecharge.spring.ioc.annotations.config;

import com.freecharge.spring.ioc.annotations.Address;
import com.freecharge.spring.ioc.annotations.Employee;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import java.util.Arrays;
import java.util.List;

@Configuration
@ComponentScan(basePackages = "com.freecharge.spring.ioc.annotations")
public class AppConfig {

    /*
    Bean Definition
    Factory Methods
     */
    /*
   @Bean(name = "emp1" , initMethod = "init", destroyMethod = "cleanup")
   @Scope("prototype")
    public Employee getEmployee(){
        Employee employee = new Employee(111,"emp1","emp1@gmail.com","2323423", Arrays.asList(getHomeAddress(),getOfficeAddress()));
        return employee;
    } */
    @Bean(name = "homeaddress")
    public Address getHomeAddress(){
       return new Address("Hitecy city road", "Hyderabad", 23423);
    }

    @Bean(name="officeaddress")
    public Address getOfficeAddress(){
       return new Address("JNTU Road", "Hyderabad",234234);
    }
   /* @Bean("homeaddressList")
    public List<Address> getHomeAddressList(){

    }
    @Bean("officeaddressList")
    public List<Address> getOfficeAddressList(){

    }*/
}
