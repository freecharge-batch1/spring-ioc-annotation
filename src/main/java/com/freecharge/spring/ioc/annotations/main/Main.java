package com.freecharge.spring.ioc.annotations.main;

import com.freecharge.spring.ioc.annotations.Employee;
import com.freecharge.spring.ioc.annotations.config.AppConfig;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(
                new Class[]{AppConfig.class}
        );

        Employee emp1 = context.getBean(Employee.class);
        System.out.println(emp1);
    }
}
